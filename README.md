# Test task | Tribes Banking Project

### Pre-requirements

To follow the setting up instruction you should have installed:
1. Docker - [installation instructions](https://docs.docker.com/engine/install/)
2. Docker Compose - [installation instructions](https://docs.docker.com/compose/)

### How to set up the project

1. Clone the project:
```shell
git clone https://gitlab.com/pavel.savushkin.mix/tribes-test-task.git; \
cd tribes-test-task 
```
2. Clone Laradock:
```shell
git clone https://github.com/Laradock/laradock.git; \
cp .env.example .env;
```
3. (optional) Change ports/credentials if you need to (in `laradock/.env`).
4. Copy and edit the `.env` of the project:
```shell
cp .env.example .env;
```
Changes:
```dotenv
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=default
DB_USERNAME=default
DB_PASSWORD=secret
```
5. Run the services:
```shell
cd laradock; \
docker-compose up -d mysql nginx php-fpm; \
cd ..
```
6. Run migrations:
```shell
cd laradock; \
docker-compose exec workspace php artisan migrate --seed; \
cd ..
```

Now you are able to sign in as `test1@gmail.com` (password is `password` for that user).

By default, site would be available on http://127.0.0.1:80.
