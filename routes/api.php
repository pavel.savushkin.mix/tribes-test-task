<?php

use App\Http\Controllers\ChatController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/{chat_id}/messages', [ChatController::class, 'fetchMessages'])->name('messages.fetch');
});
