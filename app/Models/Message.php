<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Message extends Model
{
    use HasFactory;

    /**
     * @var int
     */
    protected $perPage = 50;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', fn($builder) => $builder->orderBy('created_at'));
    }

    /**
     * @var string[]
     */
    protected $fillable = [
        'chat_id',
        'user_id',
        'body',
    ];

    /**
     * Returns user of the message.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns chat of the message.
     *
     * @return BelongsTo
     */
    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class);
    }
}
