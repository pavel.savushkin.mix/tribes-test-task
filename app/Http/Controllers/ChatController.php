<?php

namespace App\Http\Controllers;

use App\Dto\Message as MessageEntity;
use App\Http\Requests\IndexChatRequest;
use App\Http\Requests\PostMessageRequest;
use App\Http\Resources\MessageResource;
use App\Jobs\SaveMessageJob;
use App\Models\Message;
use App\Models\Message as MessageModel;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ChatController extends Controller
{
    /**
     * Shows chat page.
     *
     * @param IndexChatRequest $request
     *
     * @return View
     */
    public function index(IndexChatRequest $request): View
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = auth()->user();
        $authenticatedUser->load([
            'messages' => fn ($builder) => $builder->limit((new MessageModel())->getPerPage()),
            'messages.chat.participants',
        ]);
        $chats = $authenticatedUser->messages->pluck('chat')->unique();
        $activeChartId = (int)$request->get('chat_id', $chats->first()->id ?? 0);
        $messages = $chats->where('id', $activeChartId)->first()->messages ?? collect();

        return view('chat', [
            'authenticatedUserId' => $authenticatedUser->id,
            'name' => $authenticatedUser->name,
            'chats' => $chats,
            'activeChartId' => $activeChartId,
            'messages' => $messages,
        ]);
    }

    /**
     * Saves the provided message.
     *
     * @param PostMessageRequest $request
     *
     * @return RedirectResponse
     */
    public function postMessage(PostMessageRequest $request): RedirectResponse
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = auth()->user();
        $chatId = $request->get('chat_id');
        $body = $request->get('body');

        $message = new MessageEntity(
            chatId: $chatId,
            userId: $authenticatedUser->id,
            body: $body,
        );

        dispatch(new SaveMessageJob($message));

        return redirect()->route('chat', ['chat_id' => $chatId]);
    }

    /**
     * Fetches latest messages of the chat.
     *
     * @return AnonymousResourceCollection
     */
    public function fetchMessages(int $chatId)
    {
        $messages = MessageModel::query()
            ->where('chat_id', $chatId)
            ->limit((new MessageModel())->getPerPage())
            ->get();

        return MessageResource::collection($messages);
    }
}
