<?php

namespace App\Dto;

final readonly class Message
{
    /**
     * @param int $chatId
     * @param int $userId
     * @param string $body
     */
    public function __construct(
        public int    $chatId,
        public int    $userId,
        public string $body,
    )
    {
    }
}
