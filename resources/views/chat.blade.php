@extends('layout')

@section('content')
    <div class="bg-gray-100 grid h-screen place-items-center">
        <div class="bg-white border rounded grid grid-cols-[1fr,2fr] w-11/12 md:w-10/12 xl:w-8/12">
            <div class="border-r">
                <ul class="list-none">
                    @foreach($chats as $chat)
                        <li>
                            <a
                                href="{{ route('chat', ['chat_id' => $chat->id]) }}"
                                class="block p-4 {{ $activeChartId === $chat->id ? 'bg-gray-200 hover:bg-gray-300' : 'hover:bg-gray-200' }}"
                            >
                                {{ Str::limit($chat->participants->pluck('name')->implode(','), 20) }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <form method="post" action="{{ route('chat.message') }}">
                {{ csrf_field() }}
                <input type="hidden" name="chat_id" value="{{ $activeChartId }}">
                <div class="relative flex items-center p-3 border-b border-gray-300">
                    <img class="object-cover w-10 h-10 rounded-full"
                         src="{{ Vite::asset('resources/images/user.png') }}"
                         alt="username"/>
                    <span class="block ml-2 font-bold text-gray-600">{{ $name }}</span>
                    <span class="absolute w-3 h-3 bg-green-600 rounded-full left-10 top-3"></span>
                </div>
                <div class="messages-list relative w-full p-6 overflow-y-auto h-[40rem]">
                    <ul class="space-y-2">
                        @foreach($messages as $message)
                            @php
                                $messageByAuthenticatedUser = $message->user_id === $authenticatedUserId;
                            @endphp
                            <li class="flex {{ $messageByAuthenticatedUser ? 'justify-end' : 'justify-start' }}">
                                <div class="relative max-w-xl px-4 py-2 text-gray-700 rounded shadow {{ $messageByAuthenticatedUser ? 'bg-gray-100' : '' }}">
                                    <span class="block">
                                        {{ $message->body }}
                                    </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="flex items-center justify-between w-full p-3 border-t border-gray-300">
                    <input type="text" placeholder="Message"
                           class="block w-full py-2 pl-4 mx-3 bg-gray-100 rounded-full outline-none focus:text-gray-700"
                           name="body" required/>
                    <button type="submit">
                        <svg class="w-5 h-5 text-gray-500 origin-center transform rotate-90"
                             xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 20 20" fill="currentColor">
                            <path
                                d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z"/>
                        </svg>
                    </button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        var chat = document.getElementsByClassName('messages-list')[0];

        if (typeof chat !== 'undefined') {
            chat.scrollTop = chat.scrollHeight;
        }
    </script>
@stop
