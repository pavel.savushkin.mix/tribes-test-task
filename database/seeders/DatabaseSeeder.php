<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Chat;
use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $emails = [
            'test1@gmail.com',
            'test2@gmail.com',
        ];

        foreach ($emails as $email) {
            $numberOfChats = mt_rand(3, 5);
            /** @var User $user */
            $user = User::factory()->state(['email' => $email])->create();

            for ($i = 0; $i < $numberOfChats; $i++) {
                $chat = Chat::factory()->create();
                Message::factory(mt_rand(5, 10))
                    ->for($user)
                    ->for($chat)
                    ->create();

                Message::factory(mt_rand(5, 10))
                    ->for($chat)
                    ->create();
            }
        }
    }
}
